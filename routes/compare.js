const { listCars, valueFilters } = require('../modules/cars.js');

module.exports = (fastify, opts, done) => {
  fastify.get('/', async (request, reply) => {
    //Grab filters
    const getParameters = request.query;
    //Split arrays
    for(let key of valueFilters) {
      if(getParameters[key]){
        getParameters[key] = getParameters[key].split(",");
      }
    }
    
    const cars = await listCars(getParameters);
    return reply.view('/views/compare.html', { ...cars, getParameters });
  })

  done();
}