const axios = require('axios');

const valueFilters = ['manufacturer', 'model', 'year'];

async function listCars(filters = {}) {
  //First grab the data
  let vehicles = await grabData();

  //Format price and year
  vehicles = vehicles.map(car => {
    return {
      ...car,
      formattedPrice: car.price.toLocaleString('nl-NL', { style: 'currency', currency: car.price_currency }),
      year: new Date(car["manufactured_at"] * 1000).getFullYear().toString()
    };
  });

  //Filter anc calculate possible values
  return {
    vehicles: filter(vehicles, filters),
    filters: calculateFilterValues(vehicles, filters)
  }
}

function filter(vehicles, filters) {
  let filteredVehicles = [];
  //Filter by value filters
  filteredVehicles = vehicles.filter(vehicle => {
    return valueFilters.every(filter => {
      return !filters[filter] || filters[filter].includes(vehicle[filter])
    });
  });

  //Filter by price
  if (filters.minPrice && filters.maxPrice) {
    filteredVehicles = filteredVehicles.filter(vehicle => {
      return vehicle.price >= filters.minPrice && vehicle.price <= filters.maxPrice;
    });
  }

  //Sort and return
  return filteredVehicles.sort((a, b) => b.manufactured_at - a.manufactured_at);
}

function calculateFilterValues(vehicles, filters) {
  //Calculate possible values for each filter
  const filterValues = vehicles.reduce((possibleFilters, vehicle) => {
    valueFilters.forEach(filter => {
      if (!possibleFilters[filter]) possibleFilters[filter] = {}

      if (filter === 'year') {
        const year = new Date(vehicle["manufactured_at"] * 1000).getFullYear();
        possibleFilters[filter][year] = (filters[filter] || []).includes(year.toString());
      } else {
        possibleFilters[filter][vehicle[filter]] = (filters[filter] || []).includes(vehicle[filter]);
      }
    });
    return possibleFilters;
  }, {})

  //Sort those values (more complex than I expected lol)
  return Object.entries(filterValues).reduce((sortedFilters, [filter, values]) => {
    sortedFilters[filter] = Object.entries(values)
      .sort((a, b) => a[0].localeCompare(b[0]))
      .reduce((sortedValues, [valueKey, selected]) => {
        sortedValues[valueKey] = selected;
        return sortedValues;
      }, {});
    return sortedFilters;
  }, {});
}

async function grabData(){
  try {
    var request = await axios("https://gravity.nl/intake/api.php");
  }catch(err){
    throw {//will get picked up in top level error handler which returns error page :)
      message: "Could not fetch data from backend"
    }
  }

  let data = await request.data;

  if(!data.vehicles || !Array.isArray(data.vehicles)){
    throw {
      message: "Got unexpected response from backend"
    }
  }

  return data.vehicles;
}

module.exports = { listCars, valueFilters };