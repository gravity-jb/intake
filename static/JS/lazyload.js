window.addEventListener("load", () => {
  const lazyImages = document.querySelectorAll("img.lazyload");
  lazyImages.forEach(img => {
    img.src = img.dataset.src;
  });
})