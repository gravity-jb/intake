window.addEventListener("load", initFilters);

function initFilters(){
  //Checkbox filters
  const filters = document.querySelectorAll(".filterCheckbox");
  filters.forEach(function(filter){
    filter.addEventListener("change", filterChangeEvent);
  });
  
  //Price filter
  const updateButton = document.querySelector("#updatePriceButton");
  updateButton.addEventListener("click", updatePrice);
}

function filterChangeEvent(event){
  const element = event.currentTarget;
  const { filter, value } = element.dataset;
  
  var url = new URL(window.location.href);
  let filterValues = [];
  
  if(url.searchParams.has(filter)){
    filterValues = url.searchParams.get(filter).split(",");
  }

  if(element.checked){
    filterValues.push(value);
  }else{
    filterValues.splice(filterValues.indexOf(value), 1);
  }

  if(filterValues.length > 0){
    url.searchParams.set(filter, filterValues.join(","));
  }else{
    url.searchParams.delete(filter);
  }


  window.location.href = url.toString();
}

function updatePrice(){
  const min = document.querySelector("#minPrice").value;
  const max = document.querySelector("#maxPrice").value;

  var url = new URL(window.location.href);
  url.searchParams.set("minPrice", min);
  url.searchParams.set("maxPrice", max);

  window.location.href = url.toString();
}