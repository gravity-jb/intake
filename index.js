const fastify = require('fastify')();
const path = require('path');

//Register view renderer with template engine ejs
fastify.register(require('point-of-view'), {
  engine: {
    nunjucks: require('nunjucks')
  }
});

//Register static files
fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'static'),
  prefix: "/static/",
})

//Register routes
fastify.register(require('./routes/root'), {prefix: '/'});
fastify.register(require('./routes/compare'), {prefix: '/compare'});

fastify.setErrorHandler(function (error, request, reply) {
  this.log.error(error)
  return reply.status(500).view('/views/error.html', {error: error.message || ""});
})


fastify.listen(3000, (err, address) => {
  if (err) throw err
})